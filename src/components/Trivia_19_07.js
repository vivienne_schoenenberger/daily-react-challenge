
import '../App.css';
import { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import styled from 'styled-components';

const Section = styled.div`

background-color: #282c34;
height : 100vh;
font-family: Montserrat;
scroll-snap-align: center;
position:relative;
z-index:2;
overflow:hidden;
`

const Trivia=()=> {

    const questions = [
      {id: 1, question: "What is the capital of France?", answer:"Paris"},
      {id: 2, question: "What is the largest ocean on Earth?", answer:"The Pacific Ocean"},
      {id: 3, question: "What is the capital of Spain?", answer:"Madrid"},
      {id: 4, question: "What is the largest continent on Earth?", answer:"Asia"},
      {id: 5, question: "What does “www” stand for in a website browser?", answer:"World Wide Web"},
      {id: 6, question: "What countries made up the original Axis powers in World War II?", answer:"Germany, Italy, and Japan"},
      {id: 7, question: "Who was the first woman to win a Nobel Prize (in 1903)?", answer:"Marie Curie"},
      {id: 8, question: "Which country consumes the most chocolate per capita?", answer:"Switzerland"},
      {id: 9, question: "Which country invented ice cream?", answer:"China"},
      {id: 10, question: "When Walt Disney was a child, which character did he play in his school function?", answer:"Peter Pan"},
    ];
  

  const [counter, setCounter]= useState(2);
  const [currentQuestion1, setCurrentQuestion1] = useState();
  const [currentQuestion2, setCurrentQuestion2] = useState();



  const displayAnswer =(event,id)=>{
    questions.find(obj => {
      if(obj.id === id) {
        alert(obj.answer)
        return obj.answer;
      }
    });
  }

  useEffect(()=>{
   returnQuestion1(counter-1)
    returnQuestion2(counter)
  })


  const returnQuestion1 = (id) => {
    questions.find(obj => {
    if(obj.id === id) {
      setCurrentQuestion1(obj.question)
      return obj.question;
    }
  });
  }

  const returnQuestion2 = (id) => {
    questions.find(obj => {
    if(obj.id === id) {
      setCurrentQuestion2(obj.question)
      return obj.question;
    }
  });
  }

  const addToCounter =(event,counter)=>{
    if(counter<10){
      setCounter(counter+2);
    }
    else setCounter(counter);
    
  }

  return (
    <Section>
    <div className="App">
      <header className="App-header">
        <div style={{display:'flex',flexDirection:'column', justifyContent:'center'}}>
        <h1>TRIVIA</h1>
        <p>React Challenge</p>
          <div className='btn-div'>
            <Button style={{backgroundColor:'#d07979', border:'#d07979'}} onClick={event=> displayAnswer(event,counter-1)}>{currentQuestion1}</Button>
            <Button onClick={event=> displayAnswer(event,counter)}>{currentQuestion2}</Button>
            <Button style={{backgroundColor:'#6c89b3', border:'#6c89b3'}} onClick={event => addToCounter(event,counter)}>Next</Button>
            <p>{counter/2} of 5 sets</p>
          </div>
        
        </div>
      </header>
    </div>
    </Section>
  );
}

export default Trivia;
