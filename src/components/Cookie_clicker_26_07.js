import React, { useState, useEffect } from 'react'
import cookie from '../images/cookie.png'
import styled from 'styled-components';
const Section = styled.div`

height : 100vh;
scroll-snap-align: center;
background-color: #6c61e1;

display: flex;
align-items: center;
justify-content:center;
position:relative;
z-index:1;
overflow:hidden;
font-family: Montserrat;
flex-direction:column;
`

function CookieClicker () {
 //TODO: Create a cookie clicker app that displays an additional cookie every x amount of clicks
 const[counter, setCounter]=useState(0);
 const [cookiesArray, setCookiesArray] = useState([]); 

 useEffect(() => {

  if(Number.isInteger(Math.sqrt(counter))){
    const newCookiesArray = []; 
    for (let i = 0; i < Math.sqrt(counter); i++) {
      newCookiesArray.push("cookie");
      console.log(cookiesArray)
    }
    setCookiesArray(newCookiesArray);
  }
  
  
 },[counter])

return (
  <Section>
    <h1 style={{color:'white'}}>Cookie clicker</h1>
            <p style={{color:'white'}}>React Challenge 26.07</p>
  <div className='cookies-div'>
    <img className='main-cookie'src={cookie} onClick={()=>setCounter(counter+1)}/>
    <p style={{color:'white'}}>{counter}</p>
    <div>
      {cookiesArray.map((_, index) => (
        // Use 'index' as the key to avoid React warning
        <img className='added-cookies' src={cookie} key={index} />
      ))}
    </div>
   
  </div>
  </Section>
  );
};

export default CookieClicker
