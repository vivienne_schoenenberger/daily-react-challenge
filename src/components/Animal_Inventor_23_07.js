import React, { useState } from 'react'
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import styled from 'styled-components';
const Section = styled.div`

height : 100vh;
scroll-snap-align: center;
background-color: #251944;

display: flex;
align-items: center;
justify-content:center;
position:relative;
z-index:1;
overflow:hidden;
font-family: Montserrat;
flex-direction:column;
`
const randomNumberInRange=(min, max) =>{
  // 👇️ get number between min (inclusive) and max (inclusive)
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function AnimalCreator  (){
  const animals = ['lion', 'tiger', 'bear', 'monkey', 'panda', 'zebra', 'giraffe'];
  const [animalName, setAnimalName] = useState('Click to generate');
  const createAnimal = (e)=>{
    const animal1 = animals[randomNumberInRange(0,animals.length-1)]
    const animal2 = animals[randomNumberInRange(0,animals.length-1)]
    const substring1 = animal1.substring(0,animal1.length-2);
    const substring2 = animal2.substring(2,animal2.length);
    const name = substring1+substring2;
    if(animal1!==animal2 && animalName!==name){
      setAnimalName(name)
    }
    else{
      createAnimal(e)
    }

  }  
  
  return(
    <Section>
      <div>        
        <h1 style={{color:'white'}}>Animal Inventor</h1>
        <p  style={{color:'white'}}>React Challenge 23.07</p>
        </div>
      <div className='animal-gen'>
      <p className='animal-p'>{animalName}</p>
        <Button className='animal-gen-btn' onClick={createAnimal}>New animal</Button>
      </div>

  </Section>
  );
};

export default AnimalCreator;