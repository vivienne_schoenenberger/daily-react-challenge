import AnimalCreator from './components/Animal_Inventor_23_07';
import CookieClicker from './components/Cookie_clicker_26_07';
import PigLatin from './components/Pig_lating_24_07';
import PixelArtist from './components/Pixel_Artist_20_07';
import Trivia from './components/Trivia_19_07';
import styled from 'styled-components';




const Container = styled.div`


height : 100vh;
scroll-snap-type: y mandatory;
sroll-behavior: smooth;
overflow-y: auto;
scrollbar-width: none;
&::-webkit-scrollbar{
  display:none
}
/* put on top */

position:relative;

`
const App=()=> {
  return(
<>
<Container>
<Trivia/>
<PixelArtist/>
<AnimalCreator/>
<PigLatin/>
<CookieClicker/>
</Container>
</>
  );
}
  
export default App;
