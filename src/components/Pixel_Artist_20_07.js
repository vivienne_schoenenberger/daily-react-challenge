import React, { useState } from 'react'
import styled from 'styled-components';
const Section = styled.div`

height : 100vh;
scroll-snap-align: center;
background-color: #b69df4;

display: flex;
align-items: center;
justify-content:center;
position:relative;
z-index:1;
overflow:hidden;
font-family: Montserrat;
`

const PixelArtist = () => {
const defaultColor = null;
// NEW: creating an array by using useState(Array(256).fill()) -
// this creates an array called pixels with 256 indexes
const [pixels, setPixels] = useState(Array(256).fill())
  
 const handleClick = (index) => {
   
  const currentColor = pixels[index];
  if (currentColor === 'black') {
    // Get all pixels from array
    const updatedPixels = [...pixels];
    // set the value at index = white
    updatedPixels[index] = defaultColor;
    setPixels(updatedPixels);
  } else {
    // If the current color is not black, set it to black
    const updatedPixels = [...pixels];
    updatedPixels[index] = 'black';
    setPixels(updatedPixels);
  }


 }
  return (
    <Section>
                <div style={{display:'flex',flexDirection:'column', justifyContent:'center'}}>
        <h1>Pixel Artist</h1>
        <p>React Challenge</p>
    <div className="pixel-grid"
      >
      {pixels.map((color, index) => (
        <div
          key={index}
          className="pixel"
          style={{ backgroundColor: color }}
          onClick={() => handleClick(index)}
        ></div>
      ))}
    </div>
    </div>
    </Section>
  );
};

export default PixelArtist;
