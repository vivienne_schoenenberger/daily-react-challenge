import React, { useState } from 'react'
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import styled from 'styled-components';
// NEW:
// arrays:
// const array = [] (inizializes a new arr)
// arr.push(value) (adds value to the array)
// arr.join(" ") (converts the array to a string with blank inbetween)
const Section = styled.div`

height : 100vh;
scroll-snap-align: center;
background-color: #ffa443;

display: flex;
flex-direction: column;
align-items: center;
justify-content:center;
position:relative;
z-index:1;
overflow:hidden;
font-family: Montserrat;
`


function PigLatin() {
    const [input, setInput] = useState('');


    const encode = (e, input) => {
        e.preventDefault();
        const wordArr = input.split(" ")
        const pigLatinWords = []
        for (let i = 0; i < wordArr.length; i++) {
            const word = wordArr[i];
            const firstLetter = word.substring(0, 1)
            if (firstLetter === "a" || firstLetter === "e" || firstLetter === "i" || firstLetter === "o" || firstLetter === "u") {
                const plWord1 = word + "way"
                pigLatinWords.push(plWord1)
            }
            else {
                const plWord2 = word.substring(1, word.length) + firstLetter + "ay"
                pigLatinWords.push(plWord2)
            }

        }
        const output = pigLatinWords.join(" ");
        setInput(output);
    }

    return (
        <Section>
            <h1>Pig Latin</h1>
            <p>React Challenge 24.07</p>
            <form>
                <label>
                    Enter some text to translate to Pig Latin:
                    <br />
                    <textarea value={input} onChange={e => setInput(e.target.value)}></textarea>
                </label>
                <br />
                <Button onClick={(e) => encode(e, input)}>Encode</Button>
            </form>
        </Section>
    );
}

export default PigLatin;
